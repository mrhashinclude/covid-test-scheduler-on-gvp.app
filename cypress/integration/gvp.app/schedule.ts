import config from '../../../config.json'

describe('GVP', () => {
  beforeEach(() => {
    cy.viewport(1920, 1080);
    cy.on('uncaught:exception', (err, runnable) => {
      // There's an uncaught exception loading the OpsGenie page which otherwise has no ill-effects, but kills the test.
      // Returning `false` here allows Cypress to continue.
      return false
    })

    cy.intercept('/com.gvp.site.apphome*').as('appHome')
    cy.visit('https://gvp.app/servlet/com.gvp.login')
    cy.wait(1000)
    cy.get('#vUSERNAME').type(config.username)
    cy.get('#vUSERPASSWORD').type(config.password || Cypress.env('PASSWORD'))
    cy.get('#BTNENTER').click()
    cy.wait('@appHome')
  });

  const schedule = async (date: string, kid: string) => {
    const dateOnly = parseInt(date.split('/')[1])
    cy.log(`Starting to schedule appointment for ${kid} on ${date}`)

    cy.contains('span', 'My Schedule').click()
    cy.wait(50)
    cy.contains('span', 'By Facility').click()
    cy.wait(500)

    cy.contains('Select a facility', {timeout: 5000, matchCase: false}).should('have.length', 1)
    cy.get('#W0030vSEARCHTEXT').type('living')
    cy.get('#W0030BTNREFRESH').click()
    cy.wait(5000)
    cy.contains('span', 'Living Wisdom School of Palo Alto').click()

    cy.contains('SELECT A PRODUCT', {timeout: 5000, matchCase: false}).should('have.length', 1)
    cy.contains('span', 'PCR Test (Lab)').should('be.visible').click()

    cy.contains('WHO ARE YOU SCHEDULING FOR?', {timeout: 15000, matchCase: false}).should('have.length', 1)
    cy.contains('p', kid).should('be.visible').click()

    cy.contains('Select a Date and Time Frame', {timeout: 15000, matchCase: false}).should('have.length', 1)
    cy.get('div.DatePickerContainer').should('be.visible').click()
    cy.get('div.DatePickerContainer input:first-child').clear().type(date)
    cy.contains('a.ui-state-default', dateOnly).should('be.visible').click()
    cy.wait(200)
    cy.contains('span', 'Slot').should('be.visible').click()

    cy.contains('PERSONAL INFORMATION', {timeout: 15000, matchCase: false}).should('have.length', 1).should('be.visible')
    cy.get('#W0070BTNNEXT').should('be.visible').scrollIntoView().click()
    cy.log('Clicked "next" on personal information page')
    cy.wait(2000)

    cy.contains('gx-mask', {timeout: 5000}).should('have.length', 0)
    cy.contains('INSURANCE INFORMATION', {timeout: 15000, matchCase: false}).should('have.length', 1).should('be.visible')
    cy.wait(1000)
    cy.get('#W0070BTNNEXT').should('be.visible').scrollIntoView().click()
    cy.log('Clicked "next" on insurance information page')
    cy.wait(5000)

    cy.contains('gx-mask', {timeout: 5000}).should('have.length', 0)
    cy.contains('NOTICE OF PRIVACY PRACTICES', {timeout: 15000, matchCase: false}).should('have.length', 1).should('be.visible')
    cy.get('#W0070BTNNEXT').should('be.visible').scrollIntoView().click()
    cy.wait(3000)

    cy.contains('gx-mask', {timeout: 5000}).should('have.length', 0)
    cy.contains('PATIENT INFORMED CONSENT', {timeout: 15000, matchCase: false}).should('have.length', 1).should('be.visible')

    // "draw" the "signature"
    cy.get('canvas').should('be.visible').scrollIntoView()
    cy.log("Waiting FOR YOU to draw on the signature pad")
    cy.pause()

    cy.get('#W0070BTNNEXT').should('be.visible').click()
    cy.wait(2500)
    cy.contains('confirm and send schedule information', {timeout: 5000, matchCase: false}).should('have.length', 1).should('be.visible')
    cy.log('Submitting Consent Form...')
    cy.get('#W0078BTNACTIONCONFIRM').should('be.visible').scrollIntoView().click()
    cy.wait(3000)
    cy.contains('schedule completed successfully', {timeout: 5000, matchCase: false}).should('have.length', 1).should('be.visible')
    cy.log(`Scheduled ${kid} on ${date}`)
  }

  config.dates.forEach((date) => {
    config.children.forEach((kid) => {
      it(`Scheduling for ${kid}, ${date}`, () => {
        schedule(date, kid)
      })
    })
  })

  config.retries.forEach(({date, child}) => {
    it(`Retrying for ${child} on ${date}`, () => {
      schedule(date, child)
    })
  })

  // it('Make schedules', () => {
  //   const dates = config.dates;
  //   const kids = config.children;

  //   // schedule(dates[0], kids[0])
  //   schedule(dates[0], kids[1])
  // })

});

