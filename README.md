# gvp.app Covid Test Scheduler #

This is a Cypress project to automate test scheduling on the gvp.app scheduling system (used by many schools).

This will schedule the *first available* time on the specified dates. 

Expect some TLC will be needed due to occasional flakiness with the GVP website.

This is built around Cypress (a UI testing framework), so these will appear to be UI test runs.

### How do I get set up? ###

If you are running this project yourself, you need some basic knowledge of JSON (for the configuration).

Assuming you have NodeJS and Yarn installed, run:

```
box$ yarn install
```

This will install the necessary modules into the project directory (under `node_modules/`). Once you have
edited the configuration file (`config.json`, see below), run `yarn schedule` to open up Cypress, then click
"Run 1 integration spec" (or the name of the file, `schedule.ts`) to start scheduling.

### Configuration ###

For the most part, you will only need to update the `config.json` file at the top level of the project. The structure of the
file is as follows (with annotations, these aren't valid JSON!)

```json
{
  "username": "gvp.app login (email)",  # e.g. "you@gmail.com"
  "password": "gvp.app password",       # e.g. "Correct Horse Battery Staple"

  # The "dates" and "children" array will feed a set of runs, one for each (date, child) combo.
  # If you only want to retry specific date/child combinations, set "dates" and "children" to empty arrays.
  "dates": [
    # Array of date strings, in MM/DD/YYYY format. The test script itself doesn't care
    # but the gvp.app date picker might.
    # For example
    "10/01/2021",
    "10/08/2021",
    "10/15/2021"    # No comma at the end of an array, but don't forget them in between
  ],

  "children": [
    # Array of child names, in the *exact format as they appear in GVP*.
    "Son You",
    "Daughter You",
    "Twin1 You",
    "Twin2 You"
  ],

  "retries": [
    # In case you need to retry individual date/child combinations, enter them here
    # Structured as an array of objects
    {
      "date": "09/14/2021",
      "child": "Twin1 You"
    },
    {
      "date": "10/01/2021",
      "child": "Daughter You"
    }
  ]
}
```
